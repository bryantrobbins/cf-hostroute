'use strict';

var util = require('util');
var get = require('lodash.get');
var hostRegex = /^(.*).cnote.info/g;

function inspect(arg) {
    console.log(util.inspect(arg, {showHidden: false, depth: null, colors: true}));
}

exports.handler = async function main(event, context, callback) {
    const request = event.Records[0].cf.request;

    // Check input
    inspect(request);

    // Update uri based on host header
    var host = get(request, 'headers.host[0].value');
    if(host) {
        var match = hostRegex.exec(host);
        if (match && match[1]) {
            request.uri = `/${match[1]}${request.uri}`;
            console.log(`New uri is ${request.uri}`);
        }
    }

    // Check updated request
    inspect(request);

    callback(null, request);
}
