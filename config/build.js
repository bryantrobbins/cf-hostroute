const rimraf = require('rimraf');
const fs = require('fs-extra');
const path = require('path');
const child_process = require('child_process');
const moment = require('moment');
const util = require('util');

// Load build configuration
const config = require('./build-config');
const stack = config['stack']

// Initialize info object
var info = {
    config: config
}

// argv[1] has the path the this script being executed
// Use script dir to relatively define others
var scriptDir = path.dirname(process.argv[1]);
var projectDir = path.join(scriptDir, '..');
var srcDir = path.join(scriptDir, '..', 'src');
var distDir = path.join(scriptDir, '..', 'dist');
var invokeDir = path.join(scriptDir, '..', '.invoke');

// Generate timestamp using moment
function _getTimestamp() {
    return moment().format('YYYYMMDDHHmmss');
}

// From here: https://stackoverflow.com/a/7685469/1439864
function _escapeShellArg (arg) {
    return `'${arg.replace(/'/g, `'\\''`)}'`;
}

// Look up a cloudformation output value by its key
// Used to implement Invoke
function _cfnOutput(stack, key) {
    var awsCmd = `aws cloudformation describe-stacks \
        --stack-name ${stack}`;
    var output = child_process.execSync(awsCmd, {cwd: distDir});
    var outputJson = JSON.parse(output.toString());
    return outputJson.Stacks[0].Outputs.filter(output => {
        return output.OutputKey === key
    })[0].OutputValue;
}

function dist() {
    console.log(`Creating dist directory at ${distDir}`);

    // Create dist directory
    rimraf.sync(distDir);
    fs.mkdirSync(distDir);

    // Copy staged files
    info.config.staged.forEach(file => {
        if(file.rewrite) {
            fs.copySync(path.join(projectDir, file.local), path.join(distDir, file.rewrite))
        } else {
            fs.copySync(path.join(projectDir, file), path.join(distDir, file));
        }
    })

    // Run clean install for production
    child_process.execSync('npm ci --only=production', {stdio: 'inherit', cwd: distDir});
}

function deploy() {
    var template = 'stack';
    // Copy template file
    var templateFileShort = `${template}.yaml`;
    var templateFile = path.join(projectDir, 'config', templateFileShort);
    var outputFileShort = `${template}-packaged-${_getTimestamp()}.yaml`
    fs.copyFileSync(templateFile, path.join(distDir, templateFileShort));

    // Update and stage info file
    info.name = template;
    // Adapted from https://stackoverflow.com/a/21976700/1439864
    fs.writeFileSync(path.join(distDir, 'info.js'), util.inspect(info) , 'utf-8');

    // Run cloudformation package
    var awsCmd = `aws cloudformation package \
        --template-file ${templateFileShort} \
        --s3-bucket ${info.config.bucket} \
        --output-template-file ${outputFileShort}`;
    child_process.execSync(awsCmd, {stdio: 'inherit', cwd: distDir});

    // Run cloudformation deploy
    awsCmd = `aws cloudformation deploy \
        --template-file ${outputFileShort} \
        --stack-name ${stack} \
        --capabilities CAPABILITY_IAM`;
    child_process.execSync(awsCmd, {stdio: 'inherit', cwd: distDir});
}

function invoke(infile) {
    // Reset invoke dir
    rimraf.sync(invokeDir);
    fs.mkdirSync(invokeDir);
    
    // Look up latest function name
    // Name is dynamic
    var lambda = _cfnOutput(`${stack}`, 'Function');
    var outfile = `handler-${_getTimestamp()}-outfile`;
    console.log(`Output saved to ${outfile}`);
    
    // Execute invoke - saves output to file
    awsCmd = `aws lambda invoke \
        --function-name ${lambda} \
        --log-type Tail`;

    // Add payload, if passed
    if(infile) {
        var input = _escapeShellArg(fs.readFileSync(path.join(projectDir, infile), 'utf-8'));
        awsCmd += ` --payload ${input}`;
    }
    // Add outfile
    awsCmd += ` ${outfile}`;
    console.log("Invoking with", awsCmd);
    var output = child_process.execSync(awsCmd, {cwd: invokeDir});
    var parsed = JSON.parse(output);

    // Cat the output
    console.log(">>>>>INVOKE STATUS>>>>>")
    console.log(parsed.StatusCode);
    console.log(">>>>>INVOKE STATUS>>>>>")
    console.log(">>>>>INVOKE ERROR>>>>>")
    console.log(parsed.FunctionError);
    console.log(">>>>>INVOKE ERROR>>>>>")
    console.log(">>>>>INVOKE OUTPUT>>>>>")
    console.log(fs.readFileSync(path.join(invokeDir, outfile)).toString());
    console.log(">>>>>INVOKE OUTPUT>>>>>")
    console.log(">>>>>INVOKE LOGS>>>>>")
    console.log(Buffer.from(parsed.LogResult, 'base64').toString("utf-8"));
    console.log(">>>>>INVOKE LOGS>>>>>")

}

function invokeLocal(infile) {
    var fn = 'handler';

    // Load the module
    var mod = require(srcDir);
    var event;

    if(infile) {
        event = JSON.parse(fs.readFileSync(path.join(projectDir, infile), 'utf-8'));
        console.log('Sending event', event);
    }

    // Call the function
    mod[fn](event, null, ((err, data) => {
        if(err) {
            console.log(">>>>>INVOKE ERROR>>>>>")
            console.log(err);
            console.log(">>>>>INVOKE ERROR>>>>>")
        } else {
            console.log(">>>>>INVOKE OUTPUT>>>>>")
            console.log(data);
            console.log(">>>>>INVOKE OUTPUT>>>>>")
        }
    }));
}


// Register functions here
var fn = {
    dist,
    deploy,
    invoke,
    invokeLocal
}

// Execute command with remaining args
var cmd = process.argv[2];
var argv = process.argv.slice(3);
console.log(`Running build command ${cmd}`);
fn[cmd](...argv);
